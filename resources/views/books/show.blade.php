@extends("layouts.plantilla")
@section("contenido")
    <table>
<tr><th style="text-align: center;">Photo</th>
    <th style="padding-left: 100px; text-align: center;">Module</th>
    <th style="padding-left: 100px; text-align: center;">Publisher</th>
    <th style="padding-left: 100px; text-align: center;">Price</th>
    <th style="padding-left: 100px; text-align: center;">Pages</th>
    <th style="padding-left: 100px; text-align: center;">Acciones</th></tr>


        <td><img src="{{ asset('/storage/images/' . $book->photo) }}"></td>
        <td style="padding-left: 75px;">{{$book->module->cliteral}}</td>
        <td style="padding-left: 75px;">{{$book->publisher}}</td>
        <td style="padding-left: 75px; text-align: center;">{{$book->price.'€'}}</td>
        <td style="padding-left: 75px; text-align: center;">{{$book->pages}}</td>
        <td style="padding-left: 100px;"><a href="{{route('books.destroy',$book->id)}}"><button>Borrar</button></a></td>
        <td style="padding-left: 10px;"><a href="{{route('books.edit',$book->id)}}"><button>Editar</button></a></td>
        </tr>

</table>

@endsection
