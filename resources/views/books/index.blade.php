@extends("layouts.plantilla")
@section("contenido")
<header>
        <button><a href="{{route('books.create')}}">Subir producto</a></button>
    </header>
<table>
<tr><th style="text-align: center;">Id</th><th style="padding-left: 200px; text-align: center;">Module</th><th style="padding-left: 200px; text-align: center;">Publisher</th></tr>
@foreach ($books ??[] as $book) 
        <tr>
        <td>{{$book->id}}</td>
        <td style="padding-left: 100px;">{{$book->module->cliteral}}</td>
        <td style="padding-left: 200px;">{{$book->publisher}}</td>
       <td style="padding-left: 200px;"><a href="{{route('books.show',$book->id)}}"><button>Ver más</button></a></td>
        </tr>

@endforeach
</table>

{{ $books->links() }}

@endsection
