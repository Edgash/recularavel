@extends("layouts.plantilla")
@section("contenido")
    <section class="col-md-12">
        <div class="container  d-flex justify-content-center">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="display-4">Edit Product</h1>
                </div>
            </div>
            <div class="row">
                <form method="post" action="{{ route('books.update', $book->id) }}" enctype="multipart/form-data" >
                    @csrf
                    @method('PUT')
                    <!-- Publisher input -->
                    <div class="form-outline mb-4">
                        <input type="text" name="publisher" id="formRegister1" class="form-control" value="<?=$book->publisher?>"/>
                        <label class="form-label" for="formRegister1">Publisher</label>
                    </div>
@error('publisher')
    <span class="text-danger" style="color: red">El publisher es demasiado largo o está vacío</span>
@enderror
                    <!-- Price input -->
                    <div class="form-outline mb-4">
                        <input type="number" name="price" id="formRegister2" class="form-control" value="<?=$book->price?>"/>
                        <label class="form-label" for="formRegister2">Price</label>
                    </div>
@error('price')
    <span class="text-danger" style="color: red">El precio tiene que ser númerico y no puede estar vacío</span>
@enderror                   
                    <!-- Pages input -->
                    <div class="form-outline mb-4">
                        <input type="number" name="pages" id="formRegister3" class="form-control" value="<?=$book->pages?>"/>
                        <label class="form-label" for="formRegister3">Pages</label>
                    </div>
@error('pages')
    <span class="text-danger" style="color: red">Las tiene que ser númerico y no puede estar vacío</span>
@enderror
                    <!-- Status input -->
                    <div class="form-outline mb-4">
                        <select name="status">   
                            <option value="new" <?= ($book->status === "new")?'selected':'' ?>>New</option>
                            <option value="good" <?= ($book->status === "good")?'selected':'' ?>>Good</option>
                            <option value="used" <?= ($book->status === "used")?'selected':'' ?>>Used</option>
                            <option value="bad" <?= ($book->status === "bad")?'selected':'' ?>>Bad</option>
                        </select>
                        <br>
                        <label class="form-label" for="formRegister3">Status</label>
                    </div>
                    <!-- Photo input -->
                    <div class="form-outline mb-4">
                    <input id="photo" name="photo" type="file">
                        <br>
                        <label class="form-label" for="formRegister3">{{ $book->photo }}</label>
                    </div>
                    <!-- Comments input -->
                    <div class="form-outline mb-4">
                        <input type="text" name="comments" id="formRegister6" class="form-control" value="<?=$book->comments?>"/>
                        <label class="form-label" for="formRegister3">Comments</label>
                    </div>
@error('comments')
    <span class="text-danger" style="color: red">El comentario no puede estar vacío</span>
@enderror
                    <!-- Module input -->
                    <div class="form-outline mb-4">
                        <select id="idModule" name="idModule">   
                            @foreach ($modules as $modulos) 
                                <option value="{{ $modulos->code }}" {{ ($modulos->code === $book->idModule)?'selected':'' }}> {{$modulos->cliteral }}</option>
                            @endforeach
                        </select>
                        <br>
                        <label class="form-label" for="formRegister3">Module</label>
                    </div>
                    <!-- Submit button -->
                    <button type="submit" class="btn btn-primary btn-block">Edit book</button>
                </form>
            </div>
        </div>
    </section>

@endsection