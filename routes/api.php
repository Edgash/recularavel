<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\BookController;
use App\Http\Controllers\Api\ModuleController;
use App\Http\Controllers\Api\SaleController;
use App\Http\Controllers\Api\LoginController;
use App\Http\Controllers\Api\CartController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('user',UserController::class)->middleware('auth:sanctum');
Route::apiResource('book',BookController::class)->middleware('auth:sanctum');
Route::apiResource('module',ModuleController::class)->middleware('auth:sanctum');
Route::apiResource('sale',SaleController::class)->middleware('auth:sanctum');
Route::apiResource('cart',CartController::class)->middleware('auth:sanctum');

Route::post('login', [\App\Http\Controllers\Api\LoginController::class,'login']);

Route::post('addCart', [\App\Http\Controllers\Api\CartController::class,'agregarLibros'])->middleware('auth:sanctum');
