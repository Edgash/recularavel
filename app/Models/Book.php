<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $id
 * @property int    $idUser
 * @property Date   $soldDate
 * @property string $comments
 * @property string $photo
 * @property string $publisher
 * @property string $idModule
 */

class Book extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'books';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUser', 'soldDate', 'comments', 'photo', 'status', 'pages', 'price', 'publisher', 'idModule'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int', 'idUser' => 'int', 'soldDate' => 'date', 'comments' => 'string', 'photo' => 'string', 'publisher' => 'string', 'idModule' => 'string'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'soldDate'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
    public function Module(){
        return $this->belongsTo(Module::class,'idModule','code'); 
    } 
}
