<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use Illuminate\Support\Facades\Auth;


use App\Models\Module;

class BooksController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $books = Book::paginate(10);
        return view('books.index', ['books' => $books]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
        $modules = Module::all();
        return view('books.create', compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     */
    /*public function store(Request $request){
    
            // Obtener el archivo de imagen del formulario
            $imageFile = $request->photo;
    
            // Generar un nombre único para la imagen
            $fileName = time() . '.' . $imageFile->getClientOriginalExtension();
    
            // Guardar la imagen en el almacenamiento de Laravel
        
            $imageFile->move(storage_path().'/app/public/images/', $fileName);
            
    
        $book = new Book($request->all());
        $book->idUser = Auth::user()->id;
        $book->photo = $fileName;
        $book->save();
        return redirect()->route('books.index');
    }*/

    public function store(Request $request){

    $validatedData = $request->validate([
        'publisher' => 'required|max:50',
        'price' => 'required',
        'pages' => 'required|numeric',
        'comments' => 'required',
        'status' => 'required',
        'idModule' => 'required'
    ]);

    if ($request->hasFile('photo')) {
        $imageFile = $request->file('photo');

        $fileName = time() . '.' . $imageFile->getClientOriginalExtension();

        $imageFile->move(storage_path('/app/public/images/'), $fileName);

        $book = new Book($validatedData);
        $book->idUser = Auth::user()->id;
        $book->photo = $fileName;
        $book->save();

        return redirect()->route('books.index');
    }else{
        $book = new Book($validatedData);
        $book->idUser = Auth::user()->id;
        $book->save();

        return redirect()->route('books.index');
    }

    return back()->withErrors(['photo' => 'Se requiere una imagen.'])->withInput();
}


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $book= Book::findOrFail($id);
        return view('books.show', compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $book = Book::findOrFail($id);
        $modules = Module::all();
        return view('books.edit', compact('book', 'modules'));
    }

    /**
     * Update the specified resource in storage.
     */
    /*public function update(Request $request, string $id)
    {

            // Obtener el archivo de imagen del formulario
            $imageFile = $request->photo;
    
            // Generar un nombre único para la imagen
            $fileName = time() . '.' . $imageFile->getClientOriginalExtension();
    
            // Guardar la imagen en el almacenamiento de Laravel
            
        
            $imageFile->move(storage_path().'/app/public/images/', $fileName);
            
    

        $book = Book::findOrFail($id);
        $book->update($request->all());
        $book->photo = $fileName;
        $book->save();
        return redirect()->route('books.index');
    }*/

    public function update(Request $request, string $id) {
        $validatedData = $request->validate([
            'publisher' => 'required|max:50',
            'price' => 'required',
            'pages' => 'required|numeric',
            'comments' => 'required',
            'status' => 'required',
            'idModule' => 'required'
        ]);

        if ($request->hasFile('photo')) {
            $imageFile = $request->file('photo');

            $fileName = time() . '.' . $imageFile->getClientOriginalExtension();

            $imageFile->move(storage_path('/app/public/images/'), $fileName);

            $book = Book::findOrFail($id);
            $book->update($validatedData);
            $book->photo = $fileName;
            $book->save();
            return redirect()->route('books.index');

        }else{

            $book = Book::findOrFail($id);
            $book->update($validatedData);
            $book->save();
            return redirect()->route('books.index');
        }

        return back()->withErrors(['photo' => 'Se requiere una imagen.'])->withInput();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $book = Book::find($id);
        $book->delete();
        return redirect()->route('books.index');
    }
}
