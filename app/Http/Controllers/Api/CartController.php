<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use Illuminate\Http\Request;
use App\Http\Resources\CartResource;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return CartResource::collection(Cart::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $cart = new Cart;
        $cart->idBook = $request->idBook;
        $cart->idUser = $request->idUser;        
        $cart->date = $request->date;
        $cart->status = $request->status;

        $cart->save();

        return response()->json($cart, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Cart $cart)
    {
        return new CartResource($cart);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Cart $cart)
    {
        $cart->idBook = $request->idBook;
        $cart->idUser = $request->idUser;
        $cart->date = $request->date;
        $cart->status = $request->status;

        $cart->save();
        return response()->json($cart);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Cart $cart)
    {
        $cart->delete();
        return response()->json(null, 204);
    }


    public function agregarLibros(Request $request)
    {
        $date = Carbon::now();
        $cart = $request->input('carrito');


        //return response()->json(['message' => Auth::user()]);

        foreach ($cart as $carrito) {
            Cart::create(['idBook' => $carrito, 'idUser' => Auth::user()->id, 'date' => $date, 'status' => 1]);
        }

        return response()->json(['message' => "Datos enviados correctamente"]);
    }
}
