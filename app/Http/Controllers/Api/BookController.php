<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Book;
use Illuminate\Http\Request;
use App\Http\Resources\BookResource;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return BookResource::collection(Book::all());
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $book = new Book;
        $book->idUser = $request->idUser;
        $book->idModule = $request->idModule;
        $book->publisher = $request->publisher;
        $book->price = $request->price;
        $book->pages = $request->pages;
        $book->status = $request->status;
        $book->photo = $request->photo;
        $book->comments = $request->comments;
        $book->soldDate = $request->soldDate;

        $book->save();

        return response()->json($book, 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Book $book)
    {
        return new BookResource($book);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Book $book)
    {
        $book->idUser = $request->idUser;
        $book->idModule = $request->idModule;
        $book->publisher = $request->publisher;
        $book->price = $request->price;
        $book->pages = $request->pages;
        $book->status = $request->status;
        $book->photo = $request->photo;
        $book->comments = $request->comments;
        $book->soldDate = $request->soldDate;
        $book->save();
        return response()->json($book);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return response()->json(null, 204);
    }
}
